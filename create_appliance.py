import argparse
import socket
import time

import libnfs
import ovmclient
from ovmclient import constants
import paramiko

template_name = 'coriolis-demo-base'
vm_name = 'cloudbase-demo'
assembly_name = "coriolis-demo"

vm_username = "root"
vm_password = "coriolis"


def _get_ovm_client(host, username, password):
    client = ovmclient.Client(
        'https://%s:7002/ovm/core/wsapi/rest' % host, username, password)
    client.managers.wait_for_manager_state()
    return client


def _clone_template(client, pool_name):
    pool_id = client.server_pools.get_id_by_name(pool_name)
    template_vm = client.vms.get_by_name(template_name)

    print('Cloning template "%s"' % template_name)
    job = client.jobs.wait_for_job(client.vms.clone(
        template_vm['id'], pool_id))
    vm_id = job['resultId']

    try:
        data = client.vms.get_by_id(vm_id)
        data["name"] = vm_name
        client.jobs.wait_for_job(client.vms.update(vm_id, data))

        print('Starting VM "%s"' % vm_name)
        client.jobs.wait_for_job(client.vms.start(vm_id))

        print('Waiting for IP address')

        ip = None
        while not ip:
            vnic = client.vm_virtual_nics(vm_id).get_all()[0]
            ips = [i for i in vnic['ipAddresses'] if i['type'] == 'IPV4']
            if (ips and ips[0].get('address') and
                    ips[0].get('address') != '0.0.0.0'):
                ip = ips[0]['address']
            else:
                time.sleep(1)

        return (vm_id['value'], ip)
    except:
        if vm_id:
            _delete_vm(client, vm_id)
        raise


def _delete_vm(client, vm_id):
    try:
        print('Killing VM "%s"' % vm_id)
        client.jobs.wait_for_job(client.vms.kill(vm_id))
    except Exception as ex:
        # Ignore
        print(ex)
    print('Deleting VM "%s"' % vm_id)
    client.vms.delete(vm_id)


def _wait_for_vm_to_stop(client, vm_id):
    while True:
        vm = client.vms.get_by_id(vm_id)
        if vm['vmRunState'] == 'STOPPED':
            break
        else:
            time.sleep(2)


def _export_vm_appliance(client, vm_id, repo_name):
    repo_id = client.repositories.get_id_by_name(repo_name)
    # simpleId format is needed
    vm = client.vms.get_by_id(vm_id)
    vm_id = vm["id"]

    job = client.jobs.wait_for_job(
        client.repositories.export_as_assembly(
            repo_id, assembly_name, [vm_id]))
    return job["resultId"]


def _get_local_ip_address(server_ip):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect((server_ip, 22))
    return s.getsockname()[0]


def _create_repository_export(client, pool_name, repo_name):
    repository_id = client.repositories.get_id_by_name(repo_name)
    pool_id = client.server_pools.get_id_by_name(pool_name)
    server = client.server_pool_servers(pool_id).get_all()[0]
    local_ip = _get_local_ip_address(server["ipAddress"])
    nfs_options = "ro, async, no_root_squash"

    repo_exports_mgr = client.server_repository_exports(server["id"])

    # Check if the export already exists first
    for repo_exp in repo_exports_mgr.get_all():
        if (repo_exp["repositoryId"] == repository_id and
                repo_exp["clientHostname"] == local_ip):
            if repo_exp["options"] != nfs_options:
                repo_exports_mgr.delete(repo_exp["id"])
            else:
                return repo_exp["id"]

    data = {}
    data["exportFsType"] = constants.EXPORT_FS_TYPE_NFS
    data["serverId"] = server["id"]
    data["repositoryId"] = repository_id
    data["clientHostname"] = local_ip
    data["options"] = nfs_options

    job = client.jobs.wait_for_job(repo_exports_mgr.create(data))
    return job["resultId"]


def _get_nfs_url(client, repo_export_id):
    repo_export = client.repository_exports.get_by_id(repo_export_id)
    path = repo_export["repositoryPath"]
    server = client.servers.get_by_id(repo_export['serverId'])

    # Connect using root as uid/gid
    return "nfs://%s%s?uid=0&gid=0" % (server["ipAddress"], path)


def _copy_ova(nfs_url, assembly_id, target_path):
    nfs = libnfs.NFS(str(nfs_url))
    package_path = 'Assemblies/%s/package.ova' % assembly_id["value"]
    fr = nfs.open(str(package_path), mode='rb')
    with open(target_path, 'wb') as fw:
        while True:
            buf = fr.read(10 * 1024 * 1024)
            if not buf:
                break
            fw.write(buf)
    fr.close()


def _delete_assembly(client, repo_name, assembly_id):
    repository_id = client.repositories.get_id_by_name(repo_name)
    client.jobs.wait_for_job(
        client.repository_assemblies(repository_id).delete(assembly_id))


def _get_ssh_client(ip, vm_username, vm_password):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(
        paramiko.AutoAddPolicy())
    ssh.connect(ip, username=vm_username, password=vm_password)
    return ssh


def _exec_cmd(ssh, cmd, check_exit_code=True):
    stdin, stdout, stderr = ssh.exec_command(cmd)
    print(stdout.readlines())
    print(stderr.readlines())
    exit_code = stdout.channel.recv_exit_status()
    if check_exit_code and exit_code != 0:
        raise Exception("Command exited with code: %s" % exit_code)


def _parse_args():
    parser = argparse.ArgumentParser(
        description='Create and export a Coriolis appliance.')
    parser.add_argument('--ovm-host', dest="ovm_host", help='OVM host',
                        required=True)
    parser.add_argument('--ovm-user', dest="ovm_user", help='OVM user',
                        required=True)
    parser.add_argument('--ovm-password', dest="ovm_password",
                        help='OVM password', required=True)
    parser.add_argument('--pool', dest="pool_name", help='OVM server pool',
                        required=True)
    parser.add_argument('--repo', dest="repo_name",
                        help='OVM repository', required=True)
    parser.add_argument('--docker-registry', dest="registry_host",
                        help='Docker registry', required=True)
    parser.add_argument('--docker-registry-user', dest="registry_username",
                        help='Docker registry user', required=True)
    parser.add_argument('--docker-registry-password',
                        dest="registry_password",
                        help='Docker registry user', required=True)
    parser.add_argument('--target-path', dest="target_path",
                        help='Target OVS path', required=True)
    return parser.parse_args()


def main():
    args = _parse_args()
    client = _get_ovm_client(args.ovm_host, args.ovm_user, args.ovm_password)

    vm_id, ip = _clone_template(client, args.pool_name)
    print("VM id: %s, ip: %s" % (vm_id, ip))
    try:
        ssh = _get_ssh_client(ip, vm_username, vm_password)

        _exec_cmd(
            ssh,
            "git clone https://bitbucket.org/cloudbase/coriolis-docker.git")
        _exec_cmd(
            ssh, 'docker login %s -u %s -p "%s"' % (
                args.registry_host, args.registry_username,
                args.registry_password))

        print('Deploying Kolla')
        _exec_cmd(ssh, "./coriolis-docker/kolla/deploy.sh")

        print('Deploying Coriolis')
        _exec_cmd(ssh, "./coriolis-docker/deploy.sh")

        _exec_cmd(ssh, 'docker logout %s' % args.registry_host)
        _exec_cmd(ssh, "rm -f ~/.bash_history && history -c")

        print('Powering off VM')
        _exec_cmd(ssh, "poweroff", check_exit_code=False)

        print('Waiting for the VM to stop')
        _wait_for_vm_to_stop(client, vm_id)

        print('Exporting OVA "%s"' % assembly_name)
        assembly_id = _export_vm_appliance(client, vm_id, args.repo_name)
        print("Appliance id: %s " % assembly_id)

        print("Deleting VM")
        _delete_vm(client, vm_id)
        vm_id = None

        print("Exporting repository")
        repo_export_id = _create_repository_export(
            client, args.pool_name, args.repo_name)

        print("Opening NFS")
        nfs_url = _get_nfs_url(client, repo_export_id)
        print("NFS url: %s" % nfs_url)

        print('Copying OVA to "%s"' % args.target_path)
        _copy_ova(nfs_url, assembly_id, args.target_path)

    finally:
        if assembly_id:
            print("Deleting OVA in OVM")
            _delete_assembly(client, args.repo_name, assembly_id)
        if vm_id:
            print("Deleting VM")
            _delete_vm(client, vm_id)


if __name__ == "__main__":
    main()
